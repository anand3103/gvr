const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const path = require('path');
const config = require('../../config/config').get(process.env.NODE_ENV);
const authModel = require('../../models/auth/auth.model');
const ResMessage = require('../../utils/response.util');
const CONST = require('../../constants/constant');
const { v4: uuidv4 } = require('uuid');

exports.userLogin = async (req, res) => {
    try {
        const body = req.body;
        const criteria = {
            email_id: body.email_id
        }
        const last_login_updated_date = {
            "$set": {
                "last_login_updated_date": new Date()
            }
        }
        const projection =
        {
            first_name: 1,
            last_name: 1,
            password: 1,
            email_id: 1,
            usertype_id: 1,
            user_id: 1,
            is_active: 1,
            job_title: 1
        }
        const loginData = await authModel.userLogin(criteria, projection);

        if ((loginData !== null) && (loginData !== undefined)) {
            if (loginData.is_active === false) {
                ResMessage.sendErrorCustomMessage(res, 'Your Account Has Been Blocked, Please Contact To The Admin.', 401);
            }
            const password_result = bcrypt.compareSync(body.password, loginData.password);
            if (password_result === true) {
                delete loginData["password"];
                delete loginData["_id"];
                // const last_login_update_date = authModel.lastUpdateLoginAt(criteria, last_login_updated_date);
                //const token = jwt.sign(loginData, config.JWT_SECRET_KEY, { expiresIn: config.JWT_TOKEN_EXPIRE_TIME }, { algorithm: config.JWT_TOKEN_ALGORITHM });
                const token = jwt.sign(loginData, config.JWT_SECRET_KEY);
                if (token) {
                    loginData["token"] = token;
                    ResMessage.sendSuccessCustomMessage(res, 'user login successfully', loginData, 200);
                }
            } else {
                ResMessage.sendErrorCustomMessage(res, {
                    name: "login",
                    message: 'Please Check Your Email Or Password.'
                }, 401);
            }
        } else {
            ResMessage.sendErrorCustomMessage(res, {
                name: "login",
                message: 'Please Check Your Email Or Password.'
            }, 401);
        }

    } catch (error) {
        if (error != null) {
            ResMessage.sendErrorCustomMessage(res, error, 500);
        }
        ResMessage.sendErrorCustomMessage(res, {
            name: "login",
            message: 'Please Check Your Email Or Password.'
        }, 401);
    }
}

//create new user phase 2
exports.createuser = async (req, res) => {
    try {
        const body = [
            {
                'email_id':req.body.email_id,
                'usertype_id': CONST.RESEARCH_ANALYST_USER_TYPE,
                'usertype': CONST.RESEARCH_ANALYST,
                'is_active': true,
                'user_id': uuidv4(),
                'password':await bcrypt.hash(req.body.password, config.saltRounds)
            }
        ];
        const addData = await authModel.createnewuser(body);
        if (addData.insertedCount > 0) {
            ResMessage.sendSuccessCustomMessage(res, 'user type added successfully', addData['ops'], 200);
        } else {
            ResMessage.sendErrorCustomMessage(res, 'user type not added successfully', 406);
        }

    } catch (error) {
        if (error.code == 11000) {
            let keyvalue;
            for (const key in error.keyValue) {
                keyvalue = key;
            }
            ResMessage.sendMongoErrorMessage(res, error.name, `duplicate key ${keyvalue}: ${error.keyValue[`${keyvalue}`]}`, 400);
        } else {
            ResMessage.sendErrorCustomMessage(res, error.message, 500);
        }
    }
}

exports.sendPasswordLink = async (req, res) => {
    try {
        const ejs = require('ejs');
        const cryptoRandomString = require('crypto-random-string');
        const email = require('../../utils/email.util');

        let body = req.body;
        body["password_reset_link"] = cryptoRandomString({ length: 64, type: 'url-safe' });
        const criteria = {
            "email_id": body.email_id
        };
        const condition = {
            $set: {
                password_reset_link: body["password_reset_link"],
                is_password_reset_link_available: true
            }
        };
        const updateData = await authModel.updateLinkDetails(criteria, condition);
        if (updateData) {
            const pass_url = config.PASSWORD_URL + body["password_reset_link"];
            //const template_path = path.dirname('D:/node js/grandview-phase1-backend/templates/reset_link.ejs');
            const template_path = path.dirname('/home/ubuntu/gvr-feature/templates/reset_link.ejs');
            console.log(template_path);
            ejs.renderFile(template_path + "/reset_link.ejs", { pass_url: pass_url }, (err, data) => {
                if (data && data != undefined && data != null) {
                    const email_data = {
                        from: `${config.EMAIL_ID}`, // sender address
                        to: `${body.email_id}`, // list of receivers
                        subject: "gvr Reset Password", // Subject line
                        text: "Reset Password", // plain text body
                        html: data, // html body
                    };
                    const email_response = email.sendEmail(email_data);
                    ResMessage.sendSuccessCustomMessage(res, 'Please Check Your Email For Reset Password.', '', 200);
                } else {
                    console.log(err);
                }
            });
        }
    } catch (error) {
        if (error != null) {
            ResMessage.sendErrorCustomMessage(res, error, 500);
        }
        ResMessage.sendErrorCustomMessage(res, {
            name: "forget",
            message: 'Please Check Your Email.',
        }, 401);

    }
}
exports.checkPassUrlAndResetPass = async (req, res) => {
    try {
        let body = req.body;
        const passCriteria = {
            password_reset_link: body.password_reset_link,
        };
        const projection = {
            _id: 0,
            user_id: 1,
            is_password_reset_link_available: 1,
        };
        const currentPass = await authModel.findCurPass(passCriteria, projection);
        if (currentPass && currentPass != null && currentPass != undefined) {
            if (currentPass.is_password_reset_link_available == true) {
                const hashPassword = await bcrypt.hash(body.new_password, config.saltRounds);
                const criteria = {
                    user_id: currentPass.user_id,
                };
                const condition = {
                    $set: {
                        password: hashPassword,
                        is_password_reset_link_available: false,
                        updated_at: new Date()
                    }
                };
                const updatePass = authModel.updatePass(criteria, condition);
                if (updatePass) {
                    ResMessage.sendSuccessCustomMessage(res, 'Password Updated Successfully', '', 200);
                }
            }
            else {
                ResMessage.sendErrorCustomMessage(res, {
                    message: 'Please Generate New Link For Reset Password',
                    name: 'reset'
                }, 401);
            }
        } else {
            ResMessage.sendErrorCustomMessage(res, {
                message: 'Please Generate New Link For Reset Password',
                name: 'reset'
            }, 401);
        }
    } catch (error) {
        if (error != null) {
            ResMessage.sendErrorCustomMessage(res, error.message, 500);
        } else {
            ResMessage.sendErrorCustomMessage(res, {
                message: 'Your password is not updated successfully',
                name: 'reset'
            }, 401);
        }
    }

}
