const CommonModel = require('../../models/common/common.model');
const ResMessage = require('../../utils/response.util');
const base_url = require('../../constants/constant');

//get domain list
exports.allDomainlist = async (req, res) => {
    try {
        const domainList = await CommonModel.allDomainlist();
        if (domainList) {
            ResMessage.sendSuccessCustomMessage(res, 'Domain Information.', domainList, 200);
        }
        else {
            ResMessage.sendErrorCustomMessage(res, 'No Domain List Found', 204);
        }
    } catch (error) {
        ResMessage.sendErrorCustomMessage(res, error.message, 500)
    }
}

//get industry list by domainId
exports.industrylist_by_domain_id = async (req, res, next) => {
    try {
        let domain = req.query.domainId;
        if(domain){
            const industryList = await CommonModel.industrylist_by_domain_id(domain);
            if (industryList) {
                ResMessage.sendSuccessCustomMessage(res, 'Industry Information.', industryList, 200);
            }
            else {
                ResMessage.sendErrorCustomMessage(res, 'No Industry List Found', 204);
            }
        }else{
            ResMessage.sendErrorCustomMessage(res, 'Domain ID Required', 201);
        }

    } catch (error) {
        ResMessage.sendErrorCustomMessage(res, error.message, 500)
    }
}
 
//get category by industryid
exports.categorylist_by_industry_id = async (req, res) => {
    try {
        let industry = req.query.industryId;
        if(industry){
            const categoryList = await CommonModel.categorylist_by_industry_id(industry);
            if (categoryList) {
                ResMessage.sendSuccessCustomMessage(res, 'Category Information.', categoryList, 200);
            }
            else {
                ResMessage.sendErrorCustomMessage(res, 'No Category List Found', 204);
            }
        }else{
            ResMessage.sendErrorCustomMessage(res, 'IndustryID required', 201);
        }
    } catch (error) {
        ResMessage.sendErrorCustomMessage(res, error.message, 500)
    }
}

//get last updated file by user
exports.getlastupdatedtrails = async (req, res, next) => {
    try {   
        let user = req.query.userId;     
        if(user){
            const trailslist = await CommonModel.gettrailslist(user);
            if (trailslist) {
                ResMessage.sendSuccessCustomMessage(res, 'Trail Information.', trailslist, 200);
            }
            else {
                ResMessage.sendErrorCustomMessage(res, 'No Data Found', 204);
            }
        }else{
            ResMessage.sendErrorCustomMessage(res, 'UserId Required', 201);
        }
    } catch (error) {
        ResMessage.sendErrorCustomMessage(res, error.message, 500)
    }
}

//update file updated_at value
exports.updatefilelastusedtime = async(req, res, next) => {
    try {
        let body = req.body;
        const criteria = {
            "file_id": body.fileId,
            "user_id": body.userId
        };
        const condition = {
            $set: {
                updated_at: new Date()
            }
        };
        const timeupdate = await CommonModel.updatefilelastusedtime(criteria, condition);
        if (timeupdate) {
            ResMessage.sendSuccessCustomMessage(res, 'File timestamp updated.', timeupdate, 200);
        }
        else {
            ResMessage.sendErrorCustomMessage(res, 'No Data Found', 204);
        }
    } catch (error) {
        ResMessage.sendErrorCustomMessage(res, error.message, 500)
    }
}

//get base year from config file
exports.base_year = async (req, res, next) => {
    try {   
        let yearlist = base_url.BASE_YEAR
        yearlist.forEach(element => {
            console.log(element);
          });
        ResMessage.sendSuccessCustomMessage(res, 'Trail Information.', yearlist, 200);
    } catch (error) {
        ResMessage.sendErrorCustomMessage(res, error.message, 500)
    }
}