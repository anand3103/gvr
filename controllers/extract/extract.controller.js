const ExtractModel = require('../../models/extract/extract.model');
const ResMessage = require('../../utils/response.util');

//File output with filter response
exports.extractedoutput = async (req, res, next) => {
    try {   
        let file = req.query.fileId;     
        if(file){
            const fileoutput = await ExtractModel.getfilejsonoutput(file);
            // var preout = fileoutput[0].json_output;
            // var fileId = fileoutput[0].file_id;
            // var filename = fileoutput[0].file_name;
            // var output = JSON.stringify(preout);
            // var value = output
            //                     .replace(/[\\]/g, '')
            //                     .replace(/[\\]/g, "\\")
            //                     .replace(/[\\"]/g, "'")
            //                     .replace(/[\\\/]/g, '')
            //                     .replace(/[\b]/g, '\\b')
            //                     .replace(/[\f]/g, '\\f')
            //                     .replace(/[\n]/g, '\\n')
            //                     .replace(/[\r]/g, '\\r')
            //                     .replace(/[\t]/g, '\\t');
            // var responsearray = {
            //     fileId:fileId,
            //     filename:filename,
            //     value: value
            // };
            if (fileoutput) {
                ResMessage.sendSuccessCustomMessage(res, 'File Output.', fileoutput, 200);
            }
            else {
                ResMessage.sendErrorCustomMessage(res, 'No Data Found', 204);
            }
        }else{
            ResMessage.sendErrorCustomMessage(res, 'FileId Required', 201);
        }
    } catch (error) {
        ResMessage.sendErrorCustomMessage(res, error.message, 500)
    }
}

//store domain, industry, category and baseyear based on fileId
exports.storedomaindetails = async (req, res, next) => {
    try {
        const criteria = {
            'file_id':req.body.fileid,
            "user_id": req.body.userId
        };
        const condition = {
            $set: {
                domain_name: req.body.domain,
                industry_name: req.body.industry,
                category_name:req.body.category,
                base_year:req.body.year,
                updated_at: new Date()
            }
        };

        const fileoutput = await ExtractModel.storefiledetails(criteria, condition);
        if (fileoutput) {
            ResMessage.sendSuccessCustomMessage(res, 'File Output.', value, 200);
        }
        else {
            ResMessage.sendErrorCustomMessage(res, 'No Data Found', 204);
        }
    } catch (error) {
        ResMessage.sendErrorCustomMessage(res, error.message, 500)
    }
}

//getting string request and spliting into tags
exports.gettagsrequest = async (req, res) => {

    const tagMaster = await ExtractModel.gettagmaster();
    if(tagMaster){
        var requestString = req.body.message;
        var output = []
        var json = requestString.split(' ');
        json.forEach(function (item) {
            output.push(item.replace(/\'/g, '').split(/(\d+)/).filter(Boolean));
        });
        ResMessage.sendSuccessCustomMessage(res, 'File Output.', output, 200);
    }else{
        ResMessage.sendErrorCustomMessage(res, 'No Tags Found', 204);
    }
}

//tags data add
exports.addtagsvalue = async (req, res) => {
    try {
        let body = req.body;
        let addData;
	let s1 = body["tags_name"];
        let arr_tags = [];
        s1 = s1.substring(0, s1.length);
        arr_tags = s1.split(",");
	const criteria = {
            file_id: body.file_id
        };
        const condition = {
            $push: { "tags_info": { "value": body["value"], "tags_name": arr_tags, "regions_name": body["regions_name"] } }
        }
        const conditionForLink = {
            $push: { "tags_info": { "image_link": body["image_link"], "tags_name": arr_tags, "regions_name": body["regions_name"] } }
        }

        addData = await reportWorkModel.addTagsInformation(criteria, condition);
        
        if (addData) {
            ResMessage.sendSuccessCustomMessage(res, 'tags data added successfully', 200);
        } else {
            ResMessage.sendErrorCustomMessage(res, 'tags data not added successfully', 406);
        }
    } catch (error) {
        ResMessage.sendErrorCustomMessage(res, error.message, 500);
    }
}