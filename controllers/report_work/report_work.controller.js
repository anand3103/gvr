const reportWorkModel = require('../../models/report_work/report_work.model');
const ResMessage = require('../../utils/response.util');
const CONSTANT =  require('../../constants/constant');
const shell = require('shelljs')
const { v4: uuidv4 } = require('uuid');

exports.addTagsInformation = async (req, res) => {
    try {
        let body = req.body;
	console.log("body in addTagsInformation++++++",body);
        let addData;
	let s1 = body["tags_name"];
        let arr_tags = [];
        s1 = s1.substring(0, s1.length);
        arr_tags = s1.split(",");
        let is_domain_name = false;
	const criteria = {
            file_id: body.file_id
        };
        const condition = {
            $push: { "tags_info": { "value": body["value"], "tags_name": arr_tags, "regions_name": body["regions_name"] } }
        }
        const conditionForLink = {
            $push: { "tags_info": { "image_link": body["image_link"], "tags_name": arr_tags, "regions_name": body["regions_name"] } }
        }
        if ((body.value) && (body.value != null) && (body.value != undefined)) {
            addData = await reportWorkModel.addTagsInformation(criteria, condition);
        } else if ((body.image_link) && (body.image_link != null) && (body.image_link != undefined)) {
            console.log("imageLink",body.image_link);
            addData = await reportWorkModel.addTagsInformation(criteria, conditionForLink);
        }
        if (body.domain_name) {
            const condition1 = {
                $set: {
                    domain_name: body.domain_name
                }
            }
            const setDomainName = await reportWorkModel.setDomainName(criteria, condition1);
            is_domain_name = true;
        } else {
            is_domain_name = true;
        }
        if (addData) {
            ResMessage.sendSuccessCustomMessage(res, 'tags data added successfully', { is_domain_name: is_domain_name }, 200);
        } else {
            ResMessage.sendErrorCustomMessage(res, 'tags data not added successfully', 406);
        }
    } catch (error) {
        ResMessage.sendErrorCustomMessage(res, error.message, 500);
    }
}
exports.findTagsInfoUsingUserIdAndDomainName = async (req, res) => {
    try {
        const body = req.body;
        let tagsData;
        let finalData = [];
        if ((body.type) && (body.type !== null) && (body.type !== undefined) && (body.type === 'text') && (body.regions_name === '')) {
            tagsData = await reportWorkModel.findTagsInfoUsingUserIdAndDomainNameAndText(req.body);
        } else if ((body.type) && (body.type !== null) && (body.type !== undefined) && (body.type === 'image') && (body.regions_name === '')) {
            tagsData = await reportWorkModel.findTagsInfoUsingUserIdAndDomainNameAndLink(req.body);

        } else if ((body.type) && (body.type !== null) && (body.type !== undefined) && (body.type === 'text') && (body.regions_name) && (body.regions_name !== null) && (body.regions_name !== undefined)) {
            tagsData = await reportWorkModel.findTagsInfoUsingUserIdAndDomainNameAndTextAndRegionsName(req.body);

        } else if ((body.type) && (body.type !== null) && (body.type !== undefined) && (body.type === 'image') && (body.regions_name) && (body.regions_name !== null) && (body.regions_name !== undefined)) {
            tagsData = await reportWorkModel.findTagsInfoUsingUserIdAndDomainNameAndLinkAndRegionsNames(req.body);

        }
        if (tagsData) {
            for (let i = 0; i < tagsData.length; i++) {
                if (tagsData[i].tags_value) {
                    finalData.push(tagsData[i]);
                }
            }
            ResMessage.sendSuccessCustomMessage(res, 'tags data find successfully..', finalData, 200);
        } else {
            ResMessage.sendErrorCustomMessage(res, 'tags data not found..', 204);
        }
    } catch (error) {
        ResMessage.sendErrorCustomMessage(res, error.message, 500);
    }
}
exports.findDomainsUsingUserId = async (req, res) => {
    try {
        let body = req.body;
        const domainData = await reportWorkModel.findDomainsUsingUserId(body.user_id);

        console.log(domainData);

        if (domainData) {
            let domainNames = [];
            for (let i = 0; i < domainData.length > 0; i++) {
                if (domainData[i]._id.domain_name != null) {
                    domainNames.push(domainData[i]._id);
                }
            }
            ResMessage.sendSuccessCustomMessage(res, 'tags data find successfully', domainNames, 200);
        } else {
            ResMessage.sendErrorCustomMessage(res, 'tags data not added successfully', 406);
        }
    } catch (error) {
        ResMessage.sendErrorCustomMessage(res, error.message, 500);
    }
}
exports.AddFinalReportUsingUserId = async (req, res) => {
    try {
        let body = req.body;
        body["report_id"] = uuidv4();
        const reportData = await reportWorkModel.AddFinalReportUsingUserId(body);
        console.log(reportData);
        if (reportData) {
	     shell.rm('-rf', `/home/ubuntu/gvr-file-server/upload/${body.user_id}`);
	     shell.rm('-rf', `/home/ubuntu/gvr-file-server/output/${body.user_id}`);
            ResMessage.sendSuccessCustomMessage(res, 'final report added successfully', '', 200);
        } else {
            ResMessage.sendErrorCustomMessage(res, 'final report not added successfully', 406);
        }
    } catch (error) {
        ResMessage.sendErrorCustomMessage(res, error.message, 500);
    }
}
exports.findFileNameUsingUserId = async (req, res) => {
    try {
        let body = req.body;
        const fileData = await reportWorkModel.findFileNameUsingUserId(body.user_id);
        if (fileData.length > 0) {
            ResMessage.sendSuccessCustomMessage(res, 'file data find successfully', fileData, 200);
        } else {
            ResMessage.sendSuccessCustomMessage(res, 'file data not successfully', [], 200);
        }
    } catch (error) {
        ResMessage.sendErrorCustomMessage(res, error.message, 500);
    }
}
exports.findRegionNameUsingUserIdAndDomainName = async (req, res) => {
    try {
        let body = req.body;
        const regionData = await reportWorkModel.findRegionNameUsingUserIdAndDomainName(body);
        if (regionData) {
	    let dataArr = [];
	    for(let i= 0; i < regionData.length; i++){
		if(regionData[i]._id.regions_name !== ''){
			dataArr.push({_id:regionData[i]._id});
		}
	    }
            ResMessage.sendSuccessCustomMessage(res, 'region data find successfully', dataArr, 200);
        } else {
            ResMessage.sendErrorCustomMessage(res, 'region data not successfully', 406);
        }
    } catch (error) {
        ResMessage.sendErrorCustomMessage(res, error.message, 500);
    }
}
exports.findReportDataUsingReportId = async (req, res) => {
    try {
        let body = req.body;
        const reportData = await reportWorkModel.findReportDataUsingReportId(body);
        if (reportData) {
            ResMessage.sendSuccessCustomMessage(res, 'report data find successfully', reportData, 200);
        } else {
            ResMessage.sendSuccessCustomMessage(res, 'report data not found successfully', [], 200);
        }
    } catch (error) {
        ResMessage.sendErrorCustomMessage(res, error.message, 500);
    }
}
exports.FindAllCountries = async (req, res) => {
    try {
        if (CONSTANT.COUNTRY_LIST) {
            ResMessage.sendSuccessCustomMessage(res, 'country data find successfully', CONSTANT.COUNTRY_LIST, 200);
        } else {
            ResMessage.sendErrorCustomMessage(res, 'country data not found', 406);
        }
    } catch (error) {
        ResMessage.sendErrorCustomMessage(res, error.message, 500);
    }
}
exports.FindAllDomains = async (req, res) => {
    try {
        if (CONSTANT.DOMAIN_LIST) {
            ResMessage.sendSuccessCustomMessage(res, 'domains data find successfully', CONSTANT.DOMAIN_LIST, 200);
        } else {
            ResMessage.sendErrorCustomMessage(res, 'domains data not found', 406);
        }
    } catch (error) {
        ResMessage.sendErrorCustomMessage(res, error.message, 500);
    }
}