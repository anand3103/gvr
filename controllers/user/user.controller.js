const UserModel = require('../../models/user/user.model');
const { v4: uuidv4 } = require('uuid');
const bcrypt = require('bcrypt');
const cryptoRandomString = require('crypto-random-string');
const path = require('path');
const ejs = require('ejs');
const email = require('../../utils/email.util');
const ResMessage = require('../../utils/response.util');
const config = require('../../config/config').get(process.env.NODE_ENV);


exports.addUserTypes = async (req, res) => {
    try {
        const CONST = require('../../constants/constant');
        const body = [
            {
                'usertype_id': CONST.ADMIN_USER_TYPE,
                'usertype': CONST.ADMIN,
                'is_active': true
            },
            {
                'usertype_id': CONST.RESEARCH_ANALYST_USER_TYPE,
                'usertype': CONST.RESEARCH_ANALYST,
                'is_active': true
            },
            {
                'usertype_id': CONST.CUSTOMER_USER_TYPE,
                'usertype': CONST.CUSTOMER,
                'is_active': 'true'
            }
        ];
        const addData = await UserModel.addUserTypes(body);
        if (addData.insertedCount > 0) {
            ResMessage.sendSuccessCustomMessage(res, 'user type added successfully', addData['ops'], 200);
        } else {
            ResMessage.sendErrorCustomMessage(res, 'user type not added successfully', 406);
        }
    } catch (error) {
        if (error.code == 11000) {
            let keyvalue;
            for (const key in error.keyValue) {
                keyvalue = key;
            }
            ResMessage.sendMongoErrorMessage(res, error.name, `duplicate key ${keyvalue}: ${error.keyValue[`${keyvalue}`]}`, 400);
        } else {
            ResMessage.sendErrorCustomMessage(res, error.message, 500);
        }
    }
}
exports.addSingleUser = async (req, res) => {
    try {
        const body = req.body;
        body["user_id"] = uuidv4();
        body["is_active"] = true;
        body["password"] = "welcome@123";
        const hashPassword = await bcrypt.hash(body.password, config.saltRounds);
        body["password"] = hashPassword;
        body["created_at"] = new Date();
        body["updated_at"] = new Date();
        body["is_password_reset_link_available"] = true
        body["password_reset_link"] = cryptoRandomString({ length: 64, type: 'url-safe' });
        const addData = await UserModel.addSingleUser(body);
        if (addData.insertedCount > 0) {
            delete addData['ops'][0]['password'];
           //const template_path = path.dirname('D:/node js/grandview-phase1-backend/templates/welcome.ejs');
           const template_path = path.dirname('/home/ubuntu/gvr-feature/templates/welcome.ejs');
           const pass_url = config.PASSWORD_URL + body["password_reset_link"];
            console.log(template_path);
            ejs.renderFile(template_path + "/welcome.ejs", {pass_url:pass_url}, (err, data) => {
                const email_data = {
                    from: config.EMAIL_ID, // sender address
                    to: `${body.email_id}`, // list of receivers
                    subject: "GVR Welcome Email", // Subject line
                    text: "GVR Welcome Email", // plain text body
                    html: data, // html body
                };
                const email_response = email.sendEmail(email_data);
            })
            ResMessage.sendSuccessCustomMessage(res, 'user added successfully', addData['ops'][0], 200);


        } else {
            ResMessage.sendErrorCustomMessage(res, 'user not added successfully', 406);
        }
    } catch (error) {
        if (error.code == 11000) {
            let keyvalue;
            for (const key in error.keyValue) {
                keyvalue = key;
            }
            ResMessage.sendMongoErrorMessage(res, error.name, `duplicate key ${keyvalue}: ${error.keyValue[`${keyvalue}`]}`, 400);
        } else {
            ResMessage.sendErrorCustomMessage(res, error.message, 500);
        }
    }
}
exports.allUserList = async (req, res) => {
    try {
        const projection = {
            _id: 0,
            password: 0,
            is_password_reset_link_available:0,
            password_reset_link:0
        }
        const userData = await UserModel.allUserList(projection);
        if (userData) {
            ResMessage.sendSuccessCustomMessage(res, 'User data find successfully.', userData, 200);
        }
        else {
            ResMessage.sendErrorCustomMessage(res, 'User not found into the database.', 204);
        }
    } catch (error) {
        ResMessage.sendErrorCustomMessage(res, error.message, 500)
    }
}

exports.singleUserDetails = async (req, res) => {
    try {
        const projection = {
            _id: 0,
            password: 0,
            is_password_reset_link_available: 0,
            password_reset_link: 0
        }
         const userData = await UserModel.singleUserDetails(req.params.user_id, projection);
        if (userData) {
            ResMessage.sendSuccessCustomMessage(res, 'User data find successfully.', userData, 200);
        }
        else {
            ResMessage.sendErrorCustomMessage(res, 'User not found into the database.', 204);
        }
    } catch (error) {
        ResMessage.sendErrorCustomMessage(res, error.message, 500)
    }
}
