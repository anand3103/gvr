const CONSTANT = require('../../constants/constant');
const MongoUtil = require('../../utils/mongo.util');


exports.userLogin = (criteria, projection) => {
    return new Promise((resolve, reject) => {
        MongoUtil.getDatabase().collection(CONSTANT.COLLECTION_USERS).findOne(criteria, { projection: projection }, (err, user_data) => {
            if (user_data || user_data === null) {
                resolve(user_data);
            } else {
                reject(err);
            }
        });
    });
}

exports.createnewuser = (data) => {
    return new Promise((resolve, reject) => {
        MongoUtil.getDatabase().collection(CONSTANT.COLLECTION_USERS).insertOne(  {
            email_id: data[0].email_id,
            usertype_id: data[0].usertype_id,
            usertype: data[0].usertype,
            is_active: true,
            user_id: data[0].user_id,
            password: data[0].password
          }, (err, user_data) => {
            if (user_data) {
                resolve(user_data);
            } else {
                reject(err);
            }
        });
    });
}

exports.updateLinkDetails = (criteria, condition) => {
    return new Promise((resolve, reject) => {
        MongoUtil.getDatabase().collection(CONSTANT.COLLECTION_USERS).updateOne(criteria, condition, (err, user_data) => {
            if (user_data['result']['nModified'] == 1) {
                resolve(user_data);
            }
            else {
                reject(err);
            }
        });
    });
}

exports.findCurPass = (passCriteria, projection1) => {
    return new Promise((resolve, reject) => {
        MongoUtil.getDatabase().collection(CONSTANT.COLLECTION_USERS).findOne(passCriteria,{projection:projection1},(err, user_data) => {
            if (user_data) {
                resolve(user_data);
            }
            else {
                reject(err);
            }
        });
    });
}

exports.updatePass = (criteria, condition) => {
    return new Promise((resolve, reject) => {
        MongoUtil.getDatabase().collection(CONSTANT.COLLECTION_USERS).updateOne(criteria, condition, (err, user_data) => {
            if (user_data['result']['nModified'] == 1) {
                resolve(user_data);
            }
            else {
                reject(err);
            }
        });
    });
}
