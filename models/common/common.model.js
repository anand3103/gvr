const MongoUtil = require('../../utils/mongo.util');
const CONSTANT = require('../../constants/constant');
 
exports.allDomainlist = () => {
    return new Promise((resolve, reject) => {
        MongoUtil.getDatabase().collection(CONSTANT.COLLECTION_DOMAIN_LIST).find({}).toArray((err, user_data) => {
            if (user_data) {
                resolve(user_data);
            } else {
                reject(err);
            }
        });
    });
}

exports.industrylist_by_domain_id = (domainId) => {
    return new Promise((resolve, reject) => {
        MongoUtil.getDatabase().collection(CONSTANT.COLLECTION_INDUSTRY_LIST).find({ domain_id: domainId }).toArray((err, user_data) => {
            if (user_data) {
                resolve(user_data);
            } else {
                reject(err);
            }
        });
    });
}

exports.categorylist_by_industry_id = (industryId) => {
    return new Promise((resolve, reject) => {
        MongoUtil.getDatabase().collection(CONSTANT.COLLECTION_CATEGORY_LIST).find({ industry_id: industryId}).toArray((err, user_data) => {
            if (user_data) {
                resolve(user_data);
            } else {
                reject(err);
            }
        });
    });
}

exports.gettrailslist = (userid) => {
    return new Promise((resolve, reject) => {
        MongoUtil.getDatabase().collection(CONSTANT.COLLECTION_REPORT_WORK).aggregate([
            {
                $match:{
                    user_id: userid
                }
            },{
                $project:{
                    _id: 0,
                    "file_name":1,
                    "file_id":1,
                    "updated_at":1
                }
            }
        ]).toArray(function (error, documents) {
            if (error) throw error;
            resolve(documents);
        });
    });
}

exports.updatefilelastusedtime = (criteria, condition) => {
    return new Promise((resolve, reject) => {
        MongoUtil.getDatabase().collection(CONSTANT.COLLECTION_REPORT_WORK).updateOne(criteria, condition, (err, user_data) => {
            if (user_data['result']['nModified'] == 1) {
                resolve(user_data);
            }
            else {
                reject(err);
            }
        });
    });
}
