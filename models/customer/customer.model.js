const CONSTANT = require('../../constants/constant');
const MongoUtil = require('../../utils/mongo.util');

exports.findDomainNameInFinalReport = () => {
    return new Promise((resolve, reject) => {
        MongoUtil.getDatabase().collection(CONSTANT.COLLECTION_FINAL_REPORTS).aggregate([
            {
                $group: {
                    _id: "$domain_name"
                }
            }
        ]).toArray(function (error, documents) {
            if (error) throw error;
            resolve(documents);
        });
    });
}

exports.findFileNameUsingDomainName = (domain_name) => {
    return new Promise((resolve, reject) => {
        MongoUtil.getDatabase().collection(CONSTANT.COLLECTION_FINAL_REPORTS).aggregate([
            {
                $match:{
                    domain_name: domain_name
                }
            },{
                $project:{
                    _id: 0,
                    "file_name":1,
                    "report_id":1
                }
            }
        ]).toArray(function (error, documents) {
            if (error) throw error;
            resolve(documents);
        });
    });
}
