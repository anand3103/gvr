const MongoUtil = require('../../utils/mongo.util');
const CONSTANT = require('../../constants/constant');
 
exports.getfilejsonoutput = (fileId) => {
    return new Promise((resolve, reject) => {
        MongoUtil.getDatabase().collection(CONSTANT.COLLECTION_REPORT_WORK).aggregate([
            {
                $match:{
                    file_id: fileId
                }
            },{
                $project:{
                    _id: 0,
                    "file_name":1,
                    "file_id":1,
                    "json_output":1
                }
            }
        ]).toArray(function (error, documents) {
            if (error) throw error;
            resolve(documents);
        });
    });
}

exports.storefiledetails = (criteria, condition) => {
    return new Promise((resolve, reject) => {
        MongoUtil.getDatabase().collection(CONSTANT.COLLECTION_REPORT_WORK).updateOne(criteria, condition, (err, user_data) => {
            if (user_data['result']['nModified'] == 1) {
                resolve(user_data);
            }
            else {
                reject(err);
            }
        });
    });
}

//Get the All Master Entry Of HTML Tags
exports.gettagmaster = () => {
    return new Promise((resolve, reject) => {
        MongoUtil.getDatabase().collection(CONSTANT.COLLECTION_HTML_TAGS_MASTER).find({}).toArray((err, user_data) => {
            if (user_data) {
                resolve(user_data);
            } else {
                reject(err);
            }
        });
    });
}


//store tags value 

exports.addTagsInformation = (condition, criteria) => {
    return new Promise((resolve, reject) => {
        MongoUtil.getDatabase().collection(CONSTANT.COLLECTION_REPORT_WORK).update(condition, criteria, (err, user_data) => {
            if (user_data) {
                resolve(user_data);
            } else {
                reject(err);
            }
        });
    });
}