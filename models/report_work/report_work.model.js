const CONSTANT = require('../../constants/constant');
const MongoUtil = require('../../utils/mongo.util');


exports.addTagsInformation = (condition, criteria) => {
    return new Promise((resolve, reject) => {
        MongoUtil.getDatabase().collection(CONSTANT.COLLECTION_REPORT_WORK).update(condition, criteria, (err, user_data) => {
            if (user_data) {
                resolve(user_data);
            } else {
                reject(err);
            }
        });
    });
}
exports.findTagsInfoUsingUserIdAndDomainNameAndText = (body) => {
    return new Promise((resolve, reject) => {
        MongoUtil.getDatabase().collection(CONSTANT.COLLECTION_REPORT_WORK).aggregate([
            {
                $match: {
                    $and: [
                        { "user_id": body.user_id },
                        { "domain_name": body.domain_name }
                    ]
                }
            },
            {
                $unwind: "$tags_info"
            },
            {
                $addFields: {
                    "tags_name": {
                        $reduce: {
                            input: "$tags_info.tags_name",
                            initialValue: "",
                            in: {
                                "$cond": {
                                    if: {
                                        "$eq": [
                                            {
                                                "$indexOfArray": [
                                                    "$tags_info.tags_name",
                                                    "$$this"
                                                ]
                                            },
                                            0
                                        ]
                                    },
                                    then: {
                                        "$concat": [
                                            "$$value",
                                            "$$this"
                                        ]
                                    },
                                    else: {
                                        "$concat": [
                                            "$$value",
                                            " ",
                                            "$$this"
                                        ]
                                    }
                                }
                            }
                        }
                    },
                    "tags_regions_name": {
                        $reduce: {
                            input: "$tags_info.regions_name",
                            initialValue: "",
                            in: {
                                "$cond": {
                                    if: {
                                        "$eq": [
                                            {
                                                "$indexOfArray": [
                                                    "$tags_info.regions_name",
                                                    "$$this"
                                                ]
                                            },
                                            0
                                        ]
                                    },
                                    then: {
                                        "$concat": [
                                            "$$value",
                                            "$$this"
                                        ]
                                    },
                                    else: {
                                        "$concat": [
                                            "$$value",
                                            " ",
                                            "$$this"
                                        ]
                                    }
                                }
                            }
                        }
                    }
                }
            },
            {
                $addFields: {
                    "tags_value": "$tags_info.value"
                }
            },
            {
                $project: {
                    "_id": 0,
                    "tags_value": 1,
                    "tags_name": 1,
                    "tags_regions_name": 1

                }
            },
        ]).toArray(function (error, documents) {
            if (error) throw error;
            resolve(documents);
        });
    });
}
exports.findTagsInfoUsingUserIdAndDomainNameAndLink = (body) => {
    return new Promise((resolve, reject) => {
        MongoUtil.getDatabase().collection(CONSTANT.COLLECTION_REPORT_WORK).aggregate([
            {
                $match: {
                    $and: [
                        { "user_id": body.user_id },
                        { "domain_name": body.domain_name }
                    ]
                }
            },
            {
                $unwind: "$tags_info"
            },
            {
                $addFields: {
                    "tags_name": {
                        $reduce: {
                            input: "$tags_info.tags_name",
                            initialValue: "",
                            in: {
                                "$cond": {
                                    if: {
                                        "$eq": [
                                            {
                                                "$indexOfArray": [
                                                    "$tags_info.tags_name",
                                                    "$$this"
                                                ]
                                            },
                                            0
                                        ]
                                    },
                                    then: {
                                        "$concat": [
                                            "$$value",
                                            "$$this"
                                        ]
                                    },
                                    else: {
                                        "$concat": [
                                            "$$value",
                                            " ",
                                            "$$this"
                                        ]
                                    }
                                }
                            }
                        }
                    },
                    "tags_regions_name": {
                        $reduce: {
                            input: "$tags_info.regions_name",
                            initialValue: "",
                            in: {
                                "$cond": {
                                    if: {
                                        "$eq": [
                                            {
                                                "$indexOfArray": [
                                                    "$tags_info.regions_name",
                                                    "$$this"
                                                ]
                                            },
                                            0
                                        ]
                                    },
                                    then: {
                                        "$concat": [
                                            "$$value",
                                            "$$this"
                                        ]
                                    },
                                    else: {
                                        "$concat": [
                                            "$$value",
                                            " ",
                                            "$$this"
                                        ]
                                    }
                                }
                            }
                        }
                    }
                }
            },
            {
                $addFields: {
                    "tags_value": "$tags_info.image_link"
                }
            },
            {
                $project: {
                    "_id": 0,
                    "tags_value": 1,
                    "tags_name": 1,
                    "tags_regions_name": 1
                }
            },
        ]).toArray(function (error, documents) {
            if (error) throw error;
            resolve(documents);
        });
    });
}
exports.findTagsInfoUsingUserIdAndDomainNameAndTextAndRegionsName = (body) => {
    return new Promise((resolve, reject) => {
        MongoUtil.getDatabase().collection(CONSTANT.COLLECTION_REPORT_WORK).aggregate([
            {
                $match: {
                    $and: [
                        { "user_id": body.user_id },
                        { "domain_name": body.domain_name }
                    ]
                }
            },
            {
                $unwind: "$tags_info"
            },
            {
                $match: {
                    "tags_info.regions_name": body.regions_name
                }
            },
            {
                $addFields: {
                    "tags_name": {
                        $reduce: {
                            input: "$tags_info.tags_name",
                            initialValue: "",
                            in: {
                                "$cond": {
                                    if: {
                                        "$eq": [
                                            {
                                                "$indexOfArray": [
                                                    "$tags_info.tags_name",
                                                    "$$this"
                                                ]
                                            },
                                            0
                                        ]
                                    },
                                    then: {
                                        "$concat": [
                                            "$$value",
                                            "$$this"
                                        ]
                                    },
                                    else: {
                                        "$concat": [
                                            "$$value",
                                            " ",
                                            "$$this"
                                        ]
                                    }
                                }
                            }
                        }
                    },
                    "tags_regions_name": {
                        $reduce: {
                            input: "$tags_info.regions_name",
                            initialValue: "",
                            in: {
                                "$cond": {
                                    if: {
                                        "$eq": [
                                            {
                                                "$indexOfArray": [
                                                    "$tags_info.regions_name",
                                                    "$$this"
                                                ]
                                            },
                                            0
                                        ]
                                    },
                                    then: {
                                        "$concat": [
                                            "$$value",
                                            "$$this"
                                        ]
                                    },
                                    else: {
                                        "$concat": [
                                            "$$value",
                                            " ",
                                            "$$this"
                                        ]
                                    }
                                }
                            }
                        }
                    }
                }
            },
            {
                $addFields: {
                    "tags_value": "$tags_info.value"
                }
            },
            {
                $project: {
                    "_id": 0,
                    "tags_value": 1,
                    "tags_name": 1,
                    "tags_regions_name": 1
                }
            },
        ]).toArray(function (error, documents) {
            if (error) throw error;
            resolve(documents);
        });
    });
}
exports.findTagsInfoUsingUserIdAndDomainNameAndLinkAndRegionsNames = (body) => {
    return new Promise((resolve, reject) => {
        MongoUtil.getDatabase().collection(CONSTANT.COLLECTION_REPORT_WORK).aggregate([
            {
                $match: {
                    $and: [
                        { "user_id": body.user_id },
                        { "domain_name": body.domain_name }
                    ]
                }
            },
            {
                $unwind: "$tags_info"
            },
            {
                $match: {
                    "tags_info.regions_name": body.regions_name
                }
            },
            {
                $addFields: {
                    "tags_name": {
                        $reduce: {
                            input: "$tags_info.tags_name",
                            initialValue: "",
                            in: {
                                "$cond": {
                                    if: {
                                        "$eq": [
                                            {
                                                "$indexOfArray": [
                                                    "$tags_info.tags_name",
                                                    "$$this"
                                                ]
                                            },
                                            0
                                        ]
                                    },
                                    then: {
                                        "$concat": [
                                            "$$value",
                                            "$$this"
                                        ]
                                    },
                                    else: {
                                        "$concat": [
                                            "$$value",
                                            " ",
                                            "$$this"
                                        ]
                                    }
                                }
                            }
                        }
                    },
                    "tags_regions_name": {
                        $reduce: {
                            input: "$tags_info.regions_name",
                            initialValue: "",
                            in: {
                                "$cond": {
                                    if: {
                                        "$eq": [
                                            {
                                                "$indexOfArray": [
                                                    "$tags_info.regions_name",
                                                    "$$this"
                                                ]
                                            },
                                            0
                                        ]
                                    },
                                    then: {
                                        "$concat": [
                                            "$$value",
                                            "$$this"
                                        ]
                                    },
                                    else: {
                                        "$concat": [
                                            "$$value",
                                            " ",
                                            "$$this"
                                        ]
                                    }
                                }
                            }
                        }
                    }
                }
            },
            {
                $addFields: {
                    "tags_value": "$tags_info.image_link"
                }
            },
            {
                $project: {
                    "_id": 0,
                    "tags_value": 1,
                    "tags_name": 1,
                    "tags_regions_name": 1
                }
            },
        ]).toArray(function (error, documents) {
            if (error) throw error;
            resolve(documents);
        });
    });
}
exports.setDomainName = (condition, criteria) => {
    return new Promise((resolve, reject) => {
        MongoUtil.getDatabase().collection(CONSTANT.COLLECTION_REPORT_WORK).update(condition, criteria, (err, user_data) => {
            if (user_data) {
                resolve(user_data);
            } else {
                reject(err);
            }
        });
    });
}
exports.findDomainsUsingUserId = (id) => {
    return new Promise((resolve, reject) => {
        MongoUtil.getDatabase().collection(CONSTANT.COLLECTION_REPORT_WORK).aggregate([
            {
                $match: {
                    "user_id": id,
                }
            },

            {
                $group:
                {
                    _id: { "domain_name": "$domain_name" }
                }
            },
            {
                $project: {
                    "_id.domain_name": 1
                }
            }
        ]).toArray(function (error, documents) {
            if (error) throw error;
            resolve(documents);
        });
    });
}
exports.AddFinalReportUsingUserId = (body) => {
    return new Promise((resolve, reject) => {
        MongoUtil.getDatabase().collection(CONSTANT.COLLECTION_FINAL_REPORTS).insertOne(body, (err, report_data) => {
            if (report_data) {
                resolve(report_data);
            } else {
                reject(err);
            }
        });
    });
}
exports.findFileNameUsingUserId = (id) => {
    return new Promise((resolve, reject) => {
        MongoUtil.getDatabase().collection(CONSTANT.COLLECTION_FINAL_REPORTS).aggregate([
            {
                $match: {
                    "user_id": id,
                }
            },
            {
                $project: {
                    "report_id": 1,
                    "file_name": 1
                }
            }
        ]).toArray(function (error, documents) {
            if (error) throw error;
            resolve(documents);
        });
    });
}
exports.findRegionNameUsingUserIdAndDomainName = (body) => {
    return new Promise((resolve, reject) => {
        MongoUtil.getDatabase().collection(CONSTANT.COLLECTION_REPORT_WORK).aggregate([
            {
                $match: {
                    $and: [
                        { "user_id": body.user_id },
                        { "domain_name": body.domain_name }
                    ]
                }
            },
            {
                $unwind: "$tags_info"
            },
            {
                $unwind: "$tags_info.regions_name"
            },
            {
                $group: {
                    _id: { "regions_name": "$tags_info.regions_name" }

                }
            }
        ]).toArray(function (error, documents) {
            if (error) throw error;
            console.log("==============documents===========", documents);
            resolve(documents);
        });
    });
}

exports.findReportDataUsingReportId = (body) => {
    return new Promise((resolve, reject) => {
        MongoUtil.getDatabase().collection(CONSTANT.COLLECTION_FINAL_REPORTS).aggregate([
            {
                $match: {
                    $and: [
                        { "report_id": body.report_id },
                        { "file_name": body.file_name }
                    ]
                }
            },
            {
                $project: {
                    _id: 0
                }
            }
        ]).toArray(function (error, documents) {
            if (error) throw error;
            resolve(documents);
        });
    });
}