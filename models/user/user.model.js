const MongoUtil = require('../../utils/mongo.util');
const CONSTANT = require('../../constants/constant');

exports.addUserTypes = (data) => {
    return new Promise((resolve, reject) => {
        MongoUtil.getDatabase().collection(CONSTANT.COLLECTION_USER_TYPES).insertMany(data, (err, user_data) => {
            if (user_data) {
                resolve(user_data);
            } else {
                reject(err);
            }
        });
    });
}
exports.addSingleUser = (data) => {
    return new Promise((resolve, reject) => {
        MongoUtil.getDatabase().collection(CONSTANT.COLLECTION_USERS).insertOne({ ...data }, (err, user_data) => {
            if (user_data) {
                resolve(user_data);
            } else {
                reject(err);
            }
        });
    });
}
exports.allUserList = (projection) => {
    return new Promise((resolve, reject) => {
        MongoUtil.getDatabase().collection(CONSTANT.COLLECTION_USERS).find({ $or:[{usertype_id:CONSTANT.RESEARCH_ANALYST_USER_TYPE},{usertype_id:CONSTANT.CUSTOMER_USER_TYPE}],is_active: true}, { projection: projection }).toArray((err, data) => {
            resolve(data);
        });
    })
}
exports.singleUserDetails = (id, projection) => {
    return new Promise((resolve, reject) => {
        MongoUtil.getDatabase().collection(CONSTANT.COLLECTION_USERS).findOne({ user_id: id }, { projection: projection }, (err, user_data) => {
            if (user_data || user_data == null) {
                resolve(user_data);
            } else {
                reject(err);
            }
        });
    });
}
