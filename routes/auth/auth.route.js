const express =  require('express');
const authController = require('../../controllers/auth/auth.controller');
const authValidator = require('../../validators/auth/auth.validator');

const auth = express();

auth.post('/all-user-login',[authValidator.userLogin],authController.userLogin);
auth.post('/forget-password',[authValidator.emailValidator],authController.sendPasswordLink);
auth.post('/check-password-url-reset-password/',[],authController.checkPassUrlAndResetPass);
auth.post('/register', [authValidator.newuser],authController.createuser)
module.exports = auth;