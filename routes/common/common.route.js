const express =  require('express');
const commonController = require('../../controllers/common/common.controller');
const JWTValidator = require('../../utils/jwt.util').verifyToken;

const common = express();

common.get('/domain',[JWTValidator],commonController.allDomainlist);

common.get('/industry',[JWTValidator],commonController.industrylist_by_domain_id);

common.get('/category',[JWTValidator],commonController.categorylist_by_industry_id);

common.get('/trails',[JWTValidator],commonController.getlastupdatedtrails);

common.get('/base-year', [JWTValidator], commonController.base_year);

common.post('/update-trail', [JWTValidator], commonController.updatefilelastusedtime);

module.exports = common;