const express = require('express');
const CustomerController = require('../../controllers/customer/customer.controller');
const JWTValidator = require('../../utils/jwt.util').verifyToken;

const customer = express.Router();

customer.post('/find-domains-in-final-reports', [], CustomerController.findDomainNameInFinalReport);

customer.post('/find-file-name-using-domain-name', [], CustomerController.findFileNameUsingDomainName);

customer.post('/download-pdf-file', [], CustomerController.downloadPdfFile);


module.exports = customer;
