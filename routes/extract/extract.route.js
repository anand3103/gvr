const express =  require('express');
const extractController = require('../../controllers/extract/extract.controller');
const JWTValidator = require('../../utils/jwt.util').verifyToken;

const extract = express();

extract.get('/extract-htm',[JWTValidator],extractController.extractedoutput);

extract.post('/domain-add', [JWTValidator], extractController.storedomaindetails);

module.exports = extract;