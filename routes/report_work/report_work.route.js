const express =  require('express');
const reportWorkController = require('../../controllers/report_work/report_work.controller');
const JWTValidator = require('../../utils/jwt.util').verifyToken;

const report_work = express();

report_work.post('/add-report-tags-information',[],reportWorkController.addTagsInformation);

report_work.post('/find-tags-info--to-create-report',[],reportWorkController.findTagsInfoUsingUserIdAndDomainName);

report_work.post('/find-domain-name-using-user-id',[],reportWorkController.findDomainsUsingUserId);

report_work.post('/add-final-reports',[],reportWorkController.AddFinalReportUsingUserId);

report_work.post('/find-all-file-names-particular-user',[],reportWorkController.findFileNameUsingUserId);

report_work.post('/find-all-regions-using-domain-name',[],reportWorkController.findRegionNameUsingUserIdAndDomainName);

report_work.post('/find-report-data-using-report-id',[],reportWorkController.findReportDataUsingReportId);

report_work.post('/find-all-countries-data',[],reportWorkController.FindAllCountries);

report_work.post('/find-all-domains-data',[],reportWorkController.FindAllDomains);



module.exports = report_work;