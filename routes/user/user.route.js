const express =  require('express');
const userController = require('../../controllers/user/user.controller');
const userValidator = require('../../validators/user/user.validator');
const JWTValidator = require('../../utils/jwt.util').verifyToken;

const user = express();

user.post('/add-user-types',[JWTValidator],userController.addUserTypes);
user.post('/add-single-user',[JWTValidator,userValidator.addUser],userController.addSingleUser);
user.post('/all-user-list', [JWTValidator], userController.allUserList);
user.post('/get-single-user-details/:user_id', [JWTValidator], userController.singleUserDetails);


module.exports = user;