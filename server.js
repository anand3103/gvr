const express = require('express');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const cors = require('cors');

const config = require('./config/config').get(process.env.NODE_ENV);
const mongo = require('./utils/mongo.util');
const auth = require('./routes/auth/auth.route');
const user = require('./routes/user/user.route');
const report_work = require('./routes/report_work/report_work.route');
const customer = require('./routes/customer/customer.route');
const common = require('./routes/common/common.route');
const extract = require('./routes/extract/extract.route');

const app = express();

app.use(morgan('dev'));
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
app.use(cors());

app.use('/gvr/api/user', user);
app.use('/gvr/api/auth', auth);
app.use('/gvr/api/report_work', report_work);
app.use('/gvr/api/customer', customer);
app.use('/gvr/api/common', common);
app.use('/gvr/api/extract', extract);

app.use(function (req, res, next) {
    res.status(404).send("Sorry can't find that!")
  })

mongo.dbConnection((error) => {
    if (error) {
        console.log("Error in db connection", error);
    } else {
        app.listen(config.PORT, (err) => {
            if (err) {
                console.log(err)
            }
            console.log(`Server is start on ${config.PORT}`)
        });
    }
});




