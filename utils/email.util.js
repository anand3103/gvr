const nodemailer = require("nodemailer");
const config = require('../config/config').get(process.env.NODE_ENV);

// create reusable transporter object using the default SMTP transport

exports.sendEmail = async (email_body) => {
  try {

    let transporter = nodemailer.createTransport({
      host: config.EMAIL_SERVER,
      port: config.EMAIL_PORT,
      auth: {
        user: config.EMAIL_ID,
        pass: config.EMAIL_PASSWORD,
      },
      tls: { ciphers: 'SSLv3' }
    });

    await transporter.sendMail(email_body, (err, info) => {
      if (err) {
        console.log(err);
      }
      else {
        return info;
      }
    });

  } catch (error) {
    console.log(error);
  }
}
