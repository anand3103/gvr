const jwt = require('jsonwebtoken');

const ResMessage = require('./response.util');
const config = require('../config/config').get(process.env.NODE_ENV)

exports.verifyToken = (req, res, next) => {
    try {
        const bearerHeader = req.headers['authorization'];
        if (typeof bearerHeader !== 'undefined') {
            const bearer = bearerHeader.split(' ');
            const bearerToken = bearer[1];
            const result = jwt.verify(bearerToken, config.JWT_SECRET_KEY);
            if (result) {
                next();
            } else {
                ResMessage.sendErrorCustomMessage(res, 'Authentication token is required', 401);
            }
        } else {
            ResMessage.sendErrorCustomMessage(res, 'token verfication failed', 401);
        }
    } catch (error) {
        ResMessage.sendErrorCustomMessage(res, error.message, 500);
    }
}