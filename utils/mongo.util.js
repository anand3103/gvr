const mongoClient = require('mongodb').MongoClient;
const config = require('../config/config').get(process.env.NODE_ENV);
const CONSTANT = require('../constants/constant');
let dbName;

exports.dbConnection = (callback) => {
    mongoClient.connect(config.MONGO_URL, { useUnifiedTopology: true }, (err, database) => {
        if (err) {
            return callback(err)
        }
        else {
            dbName = database.db(database.s.options.dbName);
            userCollection();
            userTypeCollection();
            reportWorkCollection();
            finalReportsCollection();
            callback(err);
        }
    });
};

function userCollection() {
    dbName.collection(CONSTANT.COLLECTION_USERS, { strict: true }, function (err, collection) {
        if (err) {
            dbName.createCollection(CONSTANT.COLLECTION_USERS, function (err, usersCollection) {
                if (usersCollection) {
                    usersCollection.createIndexes([
                        {
                            key: {
                                user_id: 1
                            },
                            unique: true,
                            sparse: true,
                            background: true
                        },
                        {
                            key: {
                                email_id: 1
                            },
                            unique: true,
                            sparse: true,
                            background: true
                        },
                        {
                            key: {
                                phone_no: 1
                            },
                            unique: true,
                            sparse: true,
                            background: true
                        }
                    ]);
                }
            });
        } else if (collection) {
            console.log("Users collection: " + collection.namespace);
        }
    });
}

function userTypeCollection() {
    dbName.collection(CONSTANT.COLLECTION_USER_TYPES, { strict: true }, function (err, collection) {
        if (err) {
            dbName.createCollection(CONSTANT.COLLECTION_USER_TYPES, function (err, usertypeCollection) {
                if (usertypeCollection) {
                    usertypeCollection.createIndexes([
                        {
                            key: {
                                usertype_id: 1
                            },
                            unique: true,
                            sparse: true,
                            background: true
                        },
                        {
                            key: {
                                usertype: 1
                            },
                            unique: true,
                            sparse: true,
                            background: true
                        },
                    ]);
                }
            });
        } else if (collection) {
            console.log("Users collection: " + collection.namespace);
        }
    });
}

function reportWorkCollection() {
    dbName.collection(CONSTANT.COLLECTION_REPORT_WORK, { strict: true }, function (err, collection) {
        if (err) {
            dbName.createCollection(CONSTANT.COLLECTION_REPORT_WORK, function (err, report) {
                if (report) {
                    report.createIndexes([
                        {
                            key: {
                                user_id: 1
                            },
                            sparse: true,
                            background: true
                        },
                        {
                            key: {
                                tags_info: 1
                            },
                            sparse: true,
                            background: true
                        },
                        {
                            key: {
                                domain_info: 1
                            },
                            sparse: true,
                            background: true
                        },
                        {
                            key: {
                                file_id: 1
                            },
                            unique: true,
                            sparse: true,
                            background: true
                        }


                    ]);
                }
            });
        } else if (collection) {
            console.log("Users collection: " + collection.namespace);
        }
    });
}

function finalReportsCollection() {
    dbName.collection(CONSTANT.COLLECTION_FINAL_REPORTS, { strict: true }, function (err, collection) {
        if (err) {
            dbName.createCollection(CONSTANT.COLLECTION_FINAL_REPORTS, function (err, report) {
                if (report) {
                    report.createIndexes([
                        {
                            key: {
                                user_id: 1
                            },
                            sparse: true,
                            background: true
                        },
                        {
                            key: {
                                report_id: 1
                            },
                            sparse: true,
                            background: true
                        },
                        {
                            key: {
                                file_name: 1
                            },
                            sparse: true,
                            unique:true,
                            background: true
                        },
                        {
                            key: {
                                domain_name: 1
                            },
                            sparse: true,
                            background: true
                        }
                    ]);
                }
            });
        } else if (collection) {
            console.log("Users collection: " + collection.namespace);
        }
    });
}

exports.getDatabase = function () {
    return dbName;
};
