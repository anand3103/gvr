const express = require('express');


exports.sendErrorCustomMessage = (res, text, errorCode) => {
    res.status(errorCode).json({
        message: text,
    });
};


exports.sendSuccessCustomMessage = (res, text, data, code) => {
    res.status(code).json({
        message: text,
        data: data
    });
};

exports.sendMongoErrorMessage = (res, err_name, err_msg, code) => {
    res.status(code).json({
        err_name: err_name,
        message: err_msg
    });
}
