const Joi = require('joi');

const resMessage = require('../../utils/response.util');

exports.userLogin = async (req, res, next) => {
    try {
        const schema = Joi.object({

            email_id: Joi.string().email().required(),
            password: Joi.string().required()
        });
        const value = await schema.validateAsync(req.body);
        if (!value) {
            resMessage.sendErrorCustomMessage(res, err["details"][0]["message"], 400);
        } else {
            next();
        }
    } catch (error) {
        console.log(error)
        resMessage.sendErrorCustomMessage(res, error.message, 400);
    }
}

exports.newuser = async (req, res, next) => {
    try {
        const schema = Joi.object({

            email_id: Joi.string().email().required(),
            password: Joi.string().required()
        });
        const value = await schema.validateAsync(req.body);
        if (!value) {
            resMessage.sendErrorCustomMessage(res, err["details"][0]["message"], 400);
        } else {
            next();
        }
    } catch (error) {
        console.log(error)
        resMessage.sendErrorCustomMessage(res, error.message, 400);
    }
}

exports.emailValidator = async (req, res, next) => {
    try {
        const schema = Joi.object({
            email_id: Joi.string().email().required(),
        });
        const value = await schema.validateAsync(req.body);
        if (!value) {
            resMessage.sendErrorCustomMessage(res, err["details"][0]["message"], 400);
        } else {
            next();
        }
    } catch (error) {
        console.log(error)
        resMessage.sendErrorCustomMessage(res, error.message, 400);
    }
}
