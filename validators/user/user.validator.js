const Joi = require('joi');
const resMessage = require('../../utils/response.util');

exports.addUser = async (req, res, next) => {
    try {
        const options = {
            allowUnknown:true
        }
        const schema = Joi.object({
            first_name: Joi.string().required(),
            last_name: Joi.string().required(),
            email_id: Joi.string().email().required(),
            mobile_no: Joi.string().required(),
            usertype_id: Joi.number().required(),
            job_title: Joi.string().required()
        });
        const value = await schema.validateAsync(req.body,options);
        if (!value) {
            resMessage.sendErrorCustomMessage(res, err["details"][0]["message"].message, 400);
        } else {
            next();
        }
    } catch (error) {
        console.log(error)
        resMessage.sendErrorCustomMessage(res, error.message, 400);
    }
}